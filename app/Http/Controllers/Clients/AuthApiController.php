<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Clients\Repo\ClientRepository;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class AuthApiController extends Controller
{

    public function login(Request $request)
    {
        $clientRepo = new ClientRepository();
        $rv = $clientRepo->login($request);
        return response()->json($rv, 200);
    }
    public function logout(Request $request)
    {
        $clientRepo = new ClientRepository();
        $rv = $clientRepo->logout($request);
        return response()->json($rv, 200);
    }
    public function getJobs(Request $request)
    {
        $clientRepo = new ClientRepository();
        $rv = $clientRepo->getJobs($request);
        return response()->json($rv, 200);
    }
    public function getApplicants(Request $request)
    {
        $clientRepo = new ClientRepository();
        $rv = $clientRepo->getApplicants($request);
        return response()->json($rv, 200);
    }
    public function getApplicantShort(Request $request, $short_code)
    {
        $clientRepo = new ClientRepository();
        $rv = $clientRepo->getApplicantShort($short_code);
        return response()->json($rv, 200);
    }
    public function applicantEntry(Request $request)
    {
        $clientRepo = new ClientRepository();
        $rv = $clientRepo->applicantEntry($request);
        return response()->json($rv, 200);
    }
    public function applicantUpdate(Request $request)
    {
        $clientRepo = new ClientRepository();
        $rv = $clientRepo->applicantUpdate($request);
        return response()->json($rv, 200);
    }
    public function questionAnswer(Request $request, $short_code)
    {
        $clientRepo = new ClientRepository();
        $rv = $clientRepo->questionAnswer($request,$short_code);
        return response()->json($rv, 200);
    }
    public function createApplicant(Request $request)
    {
        $clientRepo = new ClientRepository();
        $rv = $clientRepo->createApplicant($request);
        return response()->json($rv, 200);
    }
    public function editApplicant(Request $request)
    {
        $clientRepo = new ClientRepository();
        $rv = $clientRepo->editApplicant($request);
        return response()->json($rv, 200);
    }
    public function removeApplicant(Request $request)
    {
        $clientRepo = new ClientRepository();
        $rv = $clientRepo->removeApplicant($request);
        return response()->json($rv, 200);
    }

}

@extends('Layouts.clients')

@section('content')
    <div id="app-vue">
        <div class="container">
            <div class="row">
                <div class="col s12 m6 col offset-m3">
                    <br><br>
                    <form @submit.prevent="login">
                        <div class="card animated slideInUp">
                            <div class="card-content">
                                <span class="card-title">Login Panel</span>
                                <br>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="email" v-model="formLog.email" type="email" required class="validate">
                                        <label for="email">Email Address</label>
                                    </div>
                                    <div class="input-field col s12">
                                        <input id="password" v-model="formLog.password" type="password" required autocomplete="new-password" class="validate">
                                        <label for="password">Password</label>
                                    </div>
                                </div>
                            </div>
                            <div class="card-action right-align">
                                <button type="submit" v-if="submit == 0" class="btn waves-effect waves-light">Login</button>
                                <button type="type" v-if="submit == 1" class="btn waves-effect">Logging In ...</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        new Vue({
            el: '#app-vue',
            data: {
                api_url: '{{env("Client_API")}}',
                formLog: {
                    email : '',
                    password: ''
                },
                submit : 0
            },
            methods: {
                login: function () {
                    const _this = this;
                    const URL = this.api_url+'/auth/login';
                    _this.submit = 1;
                    $.ajax({
                        url: URL,
                        type: "post",
                        data: _this.formLog,
                        success: function (res) {
                            _this.submit = 0;
                            console.log(res);
                            if(parseInt(res.status) === 2000){
                                M.toast({html: 'Successfully logged in'});
                                window.location.href = '{{env('client.dashboard.front')}}';
                            } else {
                                M.toast({html: 'Invalid Credentials'});
                            }
                        }
                    });
                }
            }
        });
    </script>
@endsection
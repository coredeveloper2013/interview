<div id="app-applicants">
    <div class="container">
        <div class="col m8 offset-m2">
            <div class="card animated slideInUp">
                <div class="card-content">
                    <div class="card-title">
                        Applicant List
                        <a class="waves-effect waves-light btn modal-trigger right" href="#createAppModal">New Applicant</a>
                    </div>

                </div>
                <div class="card-content">
                    <table class="table" v-if="Applicants.length > 0">
                        <tr>
                            <th>Sl</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Short URL</th>
                            <th></th>
                        </tr>
                        <tr v-for="(user, index) in Applicants">
                            <td v-text="index+1"></td>
                            <td v-text="user.name"></td>
                            <td>
                                <a v-if="user.email != null" :href="'mailto:'+user.email" v-text="user.email"></a>
                                <span v-if="user.email == null"><em>No email address available</em></span>
                            </td>
                            <td>
                                <a v-if="user.phone != null" :href="'tel:'+user.phone" v-text="user.phone"></a>
                                <span v-if="user.phone == null"><em>No contact number available</em></span>
                            </td>
                            <td><a :href="'{{env('APP_URL')}}/applicant/'+user.short">Click Here</a></td>
                            <td>
                                <a class="btn-floating btn-small waves-effect waves-light blue" @click="editApplicantModal(user)"><i class="material-icons">edit</i></a>
                                <a class="btn-floating btn-small waves-effect waves-light red" @click="removeApplicant(user.id)"><i class="material-icons">delete</i></a>
                            </td>
                        </tr>
                    </table>
                </div>
                {{--<div class="card-action right-align">
                    <button type="type" class="btn waves-effect waves-light">Login</button>
                </div>--}}
            </div>
        </div>
    </div>

    <div id="createAppModal" class="modal">
        <form @submit.prevent="createApplicant">
            <div class="modal-content">
                <h4>New Applicant</h4>

                <br>
                <div class="row">
                    <div class="input-field col s12">
                        <select required v-model="formApplicant.job_id">
                            <option value="" disabled selected>Choose Job for Applicant</option>
                            <option v-for="job in jobs" :value="job.id" v-text="job.title"></option>
                        </select>
                        <label>Choose Job for Applicant <span class="pink-text">*</span></label>
                    </div>
                    <div class="input-field col s12">
                        <input id="name" v-model="formApplicant.name" type="text" required class="validate">
                        <label for="name">Applicant Name <span class="pink-text">*</span></label>
                    </div>
                    <div class="input-field col s12">
                        <input id="email" type="email" v-model="formApplicant.email" class="validate">
                        <label for="email">Email Address</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="phone" type="tel" class="validate" v-model="formApplicant.phone">
                        <label for="phone">Contact Number</label>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-red btn-flat">Cancel</a>
                <button type="submit" class="waves-effect btn">Submit</button>
            </div>
        </form>
    </div>
    <div id="editAppModal" class="modal">
        <form @submit.prevent="editApplicant">
            <div class="modal-content">
                <h4>Update Applicant</h4>

                <br>
                <div class="row">
                    <div class="input-field col s12">
                        <select required v-model="formEditApplicant.job_id">
                            <option value="" disabled selected>Choose Job for Applicant</option>
                            <option v-for="job in jobs" :value="job.id" v-text="job.title"></option>
                        </select>
                        <label>Choose Job for Applicant <span class="pink-text">*</span></label>
                    </div>
                    <div class="input-field col s12">
                        <input id="name" v-model="formEditApplicant.name" type="text" required class="validate">
                        <label for="name" :class="{active: formEditApplicant.name == '' ? false : true}">Applicant Name <span class="pink-text">*</span></label>
                    </div>
                    <div class="input-field col s12">
                        <input id="email" type="email" v-model="formEditApplicant.email" class="validate">
                        <label for="email" :class="{active: formEditApplicant.email == null ? false : true}">Email Address</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="phone" type="tel" class="validate" v-model="formEditApplicant.phone">
                        <label for="phone" :class="{active: formEditApplicant.phone == null ? false : true}">Contact Number</label>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-red btn-flat">Cancel</a>
                <button type="submit" class="waves-effect btn">Submit</button>
            </div>
        </form>
    </div>
</div>

<script>
    new Vue({
        el: '#app-applicants',
        data: {
            api_url: '{{env("Client_API")}}',
            jobs : [],
            Applicants : [],
            formApplicant: {
                job_id: '',
                name: '',
                email: '',
                phone: ''
            },
            formEditApplicant: {
                id: '',
                job_id: '',
                name: '',
                email: '',
                phone: ''
            }
        },
        methods: {
            getJobs: function () {
                const _this = this;
                const URL = this.api_url+'/clients/jobs';
                $.ajax({
                    url: URL,
                    type: "post",
                    data: {},
                    success: function (res) {
                        console.log(res);
                        if(parseInt(res.status) === 2000){
                            _this.jobs = res.data;
                            setTimeout(function(){
                                $('select').formSelect();
                            }, 100);
                        } else {
                            toastr.error('Something Wrong. Please try again!');
                        }
                    }
                });
            },
            getApplicants: function () {
                const _this = this;
                const URL = this.api_url+'/clients/applicants';
                $.ajax({
                    url: URL,
                    type: "post",
                    data: {},
                    success: function (res) {
                        console.log(res);
                        if(parseInt(res.status) === 2000){
                            _this.Applicants = res.data;
                        } else {
                            toastr.error('Something Wrong. Please try again!');
                        }
                    }
                });
            },
            createApplicant: function () {
                const _this = this;
                const URL = this.api_url+'/clients/applicant/create';
                $.ajax({
                    url: URL,
                    type: "post",
                    data: _this.formApplicant,
                    success: function (res) {
                        console.log(res);
                        if(parseInt(res.status) === 2000){
                            toastr.info(res.msg, 'New Applicant');
                            _this.getApplicants();
                            $('#createAppModal').modal('close');
                            _this.formApplicant = {
                                job_id: '',
                                name: '',
                                email: '',
                                phone: ''
                            }
                        } else {
                            toastr.error('Something Wrong. Please try again!');
                        }
                    }
                });
            },
            removeApplicant: function (id) {
                const _this = this;
                const URL = this.api_url+'/clients/applicant/remove';
                swal({
                    title: "Are you really want to remove this applicant?",
                    text: "Once deleted, you will not be able to recover this applican't information",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: URL,
                            type: "post",
                            data: {ids: [id]},
                            success: function (res) {
                                console.log(res);
                                if(parseInt(res.status) === 2000){
                                    toastr.info(res.msg, 'Remove Applicant');
                                    _this.getApplicants();
                                } else {
                                    toastr.error('Something Wrong. Please try again!');
                                }
                            }
                        });
                    } else {
                        swal.stopLoading();
                        swal.close();
                    }
                });
            },
            editApplicantModal: function(user){
                this.formEditApplicant.id = user.id;
                this.formEditApplicant.job_id = user.job_id;
                this.formEditApplicant.name = user.name;
                this.formEditApplicant.email = user.email;
                this.formEditApplicant.phone = user.phone;
                setTimeout(function(){
                    $('#editAppModal').modal('open');
                    $('select').formSelect();
                }, 100);
            },
            editApplicant: function () {
                const _this = this;
                const URL = this.api_url+'/clients/applicant/edit';
                $.ajax({
                    url: URL,
                    type: "post",
                    data: _this.formEditApplicant,
                    success: function (res) {
                        console.log(res);
                        if(parseInt(res.status) === 2000){
                            toastr.info(res.msg, 'Update Applicant');
                            _this.getApplicants();
                            $('#editAppModal').modal('close');
                        } else {
                            toastr.error('Something Wrong. Please try again!');
                        }
                    }
                });
            }
        },
        created(){
            this.getJobs();
            this.getApplicants();
        },
        mounted(){
            $(document).ready(function(){
                $('.modal').modal();
                $('select').formSelect();
            });
        }
    });
</script>
<div id="app-applicants">
    <div class="container">
        <div class="col m8 offset-m2">
            <div class="card animated slideInUp">
                <div class="card-content">
                    <div class="card-title">Applicant List</div>

                </div>
                <div class="card-content">
                    <table class="table">
                        <tr>
                            <th>Sl</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>#</td>
                            <td>Ridwanul Hafiz</td>
                            <td><a href="mailto:ridwanul.hafiz@gmail.com">ridwanul.hafiz@gmail.com</a></td>
                            <td>01717863320</td>
                            <td></td>
                        </tr>
                    </table>
                </div>
                <div class="card-action right-align">
                    <button type="type" class="btn waves-effect waves-light">Login</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    new Vue({
        el: '#app-applicants',
        data: {
            api_url: '{{env("Client_API")}}'
        },
        methods: {
            logout: function () {
                const _this = this;
                const URL = this.api_url+'/auth/logout';
                $.ajax({
                    url: URL,
                    type: "get",
                    success: function (res) {
                        console.log(res);
                        if(parseInt(res.status) === 2000){
                            window.location.href = '{{env('client.logout.api')}}';
                        } else {
                            M.toast({html: 'Something Wrong. Please try again!'});
                        }
                    }
                });
            }
        }
    });
</script>
<?php





Route::prefix('auth')->group(function () {
    Route::post('/login', 'Clients\AuthApiController@login')->name('client.login.api')->middleware('UserPanelAuthCheck');
    Route::get('/logout', 'Clients\AuthApiController@logout')->name('client.logout.api')->middleware('UserPanelCheck');
});
Route::prefix('general')->group(function () {
    Route::post('/applicant/entry', 'Clients\AuthApiController@applicantEntry')->name('client.applicant.entry.api');
    Route::post('/applicant/personal/update', 'Clients\AuthApiController@applicantUpdate')->name('client.applicant.update.api');
    Route::post('/applicant/answers/{short_code}', 'Clients\AuthApiController@questionAnswer')->name('client.applicant.question.answer.api');
    Route::get('/applicant/{short_code}', 'Clients\AuthApiController@getApplicantShort')->name('client.get.applicant.short');
});
Route::prefix('clients')->group(function () {
    Route::post('/jobs', 'Clients\AuthApiController@getJobs')->name('client.get.jobs')->middleware('UserPanelCheck');
    Route::post('/applicants', 'Clients\AuthApiController@getApplicants')->name('client.get.applicants')->middleware('UserPanelCheck');

    Route::post('/applicant/create', 'Clients\AuthApiController@createApplicant')->name('client.create.applicant')->middleware('UserPanelCheck');
    Route::post('/applicant/edit', 'Clients\AuthApiController@editApplicant')->name('client.edit.applicant')->middleware('UserPanelCheck');
    Route::post('/applicant/remove', 'Clients\AuthApiController@removeApplicant')->name('client.remove.applicant')->middleware('UserPanelCheck');
});
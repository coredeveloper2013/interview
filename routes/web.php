<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('client.login.front'));
});




Route::middleware('UserPanelAuthCheck')->prefix('auth')->group(function () {
    Route::get('/login', 'Clients\AuthController@login')->name('client.login.front');
});


Route::middleware('UserPanelCheck')->prefix('client')->group(function () {
    Route::get('/dashboard', 'Clients\AuthController@dashboard')->name('client.dashboard.front');
    Route::get('/applicants', 'Clients\AuthController@applicants')->name('client.applicants.front');
    Route::get('/jobs', 'Clients\AuthController@jobs')->name('client.jobs.front');
});



Route::get('/applicant/entry', 'Clients\AuthController@applicantEntry')->name('applicant.entry.front');
Route::get('/applicant/{short_code}', 'Clients\AuthController@applicantQuestionPaper')->name('applicant.question.front');